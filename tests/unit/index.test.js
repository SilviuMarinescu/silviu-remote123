'use strict';

var expect = require('expect');

describe('models/index', function () {
    it('returns the user', function () {
        var models = require('../../models/index');
        expect(models.User).toBeDefined();
    });

    it('returns the Book model', function () {
        var models = require('../../models');
        expect(models.Book).toBeDefined();
    });

    it('returns the Institution model', function () {
        var models = require('../../models');
        expect(models.Institution).toBeDefined();
    });
});