'use strict';

var expect = require('expect');

describe('models/user', function () {
    before(function () {
        this.timeout(50000);
        return require('../../models/index').init();
    });

    beforeEach(function () {
        this.User = require('../../models/index').User;
    });

    describe('create', function () {
        it('creates a user', function () {
            return this.User.create({
                name: 'silviu',
                email: 'silviu@test.com',
                role: 'student',
                password: '1234',
                InstitutionId: 1
            }).bind(this).then(function (user) {
                expect(user.email).toEqual('silviu@test.com');
            });
        });
    });
});