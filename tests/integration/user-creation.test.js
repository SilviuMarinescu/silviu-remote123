'use strict';

var app = require('../../index');
var expect = require('expect');
var request = require('supertest');

describe('user methods', function () {
    before(function () {
        this.timeout(50000);
        return require('../../models/index');
    });

    beforeEach(function () {
        this.timeout(50000);
        this.db = require('../../models/index');
        return this.db.User.destroy({
            truncate: true
        })
    });

    it('loads correctly the ui', function (done) {
        request(app).get('/').expect(200, done);
    });

    it('tries to log in with correct details', function (done) {
        this.db.User.create({
            name: 'silviu',
            email: 'silviu@test.com',
            role: 'student',
            password: '1234',
        }).then(function () {
            request(app).post('/users/signin')
                .send({
                    "email": "silviu@test.com",
                    "password": "1234"
                })
                .expect(200, done);
        })
    });

    it('tries to log in with incorrect details', function (done) {
        this.db.User.create({
            name: 'silviu',
            email: 'silviu@test.com',
            role: 'student',
            password: '1234',
        }).then(function () {
            request(app).post('/users/signin')
                .send({
                    "email": "silviu@test.com",
                    "password": "12345"
                })
                .expect(401, done);
        })
    });

    it('create a account for unknown domain', function (done) {
        request(app).post('/users/create')
            .send({
                "email": "silviu@test.com",
                "password": "12345",
                "name": "silviu"
            })
            .expect(/false/, done);
    });

    it('create a account for known domain', function (done) {
        request(app).post('/users/create')
            .send({
                "email": "silviu@school1.com",
                "password": "12345",
                "name": "silviu"
            })
            .expect(/true/, done);
    });

    it('gets the books from the institution of the user', function (done) {
        this.db.User.create({
            name: 'silviu',
            email: 'silviu@school1.com',
            role: 'student',
            password: '1234',
            InstitutionId: 1
        }).then(function () {
            var agent = request(app);
            agent.post('/users/signin')
                .send({
                    "email": "silviu@school1.com",
                    "password": "1234"
                }).end(function (error, response) {
                    agent.get("/users/books").set('cookie', response.headers['set-cookie']).expect(200, done);
                });
        })
    });
});