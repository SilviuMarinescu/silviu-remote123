const db = require("./models/index");

const jsend = require('jsend');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy


const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const session = require('express-session');

const express = require('express');
var hbs = require('express-hbs');
const app = express();
const index = require('./routes/index');
const users = require('./routes/users');

passport.serializeUser(function (user, done) {
    done(null, user);
});

passport.deserializeUser(function (user, done) {
    done(null, user);
});

passport.use(new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true
    },
    function (res, username, password, done) {
        db.User.findOne({
            where: {
                email: username
            }
        }).then(function (user) {
            if (!user) {
                return done(null, false);
            } else if (user.password == password) {
                res.session.user = user;
                return done(null, user);
            } else {
                return done(null, false);
            }
        });
    }
));

app.engine('hbs', hbs.express4({
    partialsDir: __dirname + '/views'
}));
app.set('view engine', 'hbs');
app.set('views', __dirname + '/views');
app.set('db', db);
app.use(bodyParser.json());
app.use(cookieParser());
app.use(jsend.middleware);
app.use(session({
    key: 'user_sid',
    secret: 'somerandonstuffs',
    resave: false,
    saveUninitialized: false,
    cookie: {
        expires: 600000
    }
}));
app.use(passport.initialize())
app.use(passport.session())

app.use((req, res, next) => {
    if (req.cookies.user_sid && !req.session.user) {
        res.clearCookie('user_sid');
    }
    next();
});

app.use('/', index);
app.use('/users', users);
app.listen(3000, () => {});

module.exports = app;