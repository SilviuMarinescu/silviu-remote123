'use strict';
module.exports = (sequelize, DataTypes) => {
    var Book = sequelize.define('Book', {
        ISBN: DataTypes.STRING,
        Title: DataTypes.STRING,
        Author: DataTypes.STRING
    });

    return Book;
};