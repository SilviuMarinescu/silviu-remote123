'use strict';
module.exports = (sequelize, DataTypes) => {
    var Institution = sequelize.define('Institution', {
        Name: DataTypes.STRING,
        URL: DataTypes.STRING,
        EmailDomain: DataTypes.STRING
    });

    Institution.associate = function (models) {
        models.Institution.hasMany(models.User);
    };

    return Institution;
};