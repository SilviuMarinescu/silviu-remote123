'use strict';
var Sequelize = require('sequelize');
var pg = require('pg');
pg.defaults.ssl = true;
var fs = require('fs');
var path = require('path');
const settings = require("../config/index");

process.env.NODE_ENV = process.env.NODE_ENV || "production";

var setings = settings[process.env.NODE_ENV].db
var db = {
    init: () => {
        return db.sequelize.sync();
    },
    dispose: () => {
        return db.sequelize.connectionManager.close();
    }
};

db.sequelize = new Sequelize(setings.database, setings.username, setings.password, {
    host: setings.host,
    dialect: 'postgres',
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
    operatorsAliases: false
});
var basename = path.basename(__filename);
fs.readdirSync(__dirname)
    .filter(file => {
        return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
    })
    .forEach(file => {
        var model = db.sequelize.import(path.join(__dirname, file));
        db[model.name] = model;
    });

Object.keys(db).forEach(modelName => {
    if (db[modelName].associate) {
        db[modelName].associate(db);
    }
});

module.exports = db;