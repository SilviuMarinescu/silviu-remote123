'use strict';
module.exports = (sequelize, DataTypes) => {
    var User = sequelize.define('User', {
        name: DataTypes.STRING,
        email: DataTypes.STRING,
        role: DataTypes.STRING,
        password: DataTypes.STRING
    });

    User.associate = function (models) {
        models.User.belongsTo(models.Institution, {
            foreignKey: {
                allowNull: false
            }
        });
    };


    return User;
};