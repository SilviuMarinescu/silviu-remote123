'use strict';
module.exports = (sequelize, DataTypes) => {
    var InstitutionBook = sequelize.define('InstitutionBook', {});
    InstitutionBook.associate = function (models) {
        models.Book.belongsToMany(models.Institution, {
            through: InstitutionBook,
        });
        models.Institution.belongsToMany(models.Book, {
            through: InstitutionBook,
        });
    };

    return InstitutionBook;
};