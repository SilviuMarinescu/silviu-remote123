const express = require('express');
const router = express.Router();
const passport = require('passport');


router.post('/signin',
    passport.authenticate('local'),
    function (req, res) {
        res.jsend.success(true);
    });

router.post('/create', function (req, res) {
    let db = req.app.get('db');
    db.Institution.findOne({
        where: {
            EmailDomain: req.body.email.split("@")[1]
        }
    }).then(function (institution) {
        if (!institution) {
            res.end("false")
        } else {
            db.User.create({
                name: req.body.name,
                email: req.body.email,
                role: 'student',
                password: req.body.password,
                InstitutionId: institution.dataValues.id
            }).then(user => {
                req.session.user = user;
                res.end("true");
            });
        }
    });
});

router.get('/books', function (req, res) {
    let db = req.app.get('db');
    db.Institution.findAll({
        through: {
            attributes: ["Name", "URL", "EmailDomain"]
        },
        where: {
            id: req.session.user.InstitutionId
        },
        include: {
            model: db.Book,
            through: {
                attributes: ["ISBN", "Title", "Author"]
            }
        }
    }).then(InstitutionBooks => {
        res.end(JSON.stringify(InstitutionBooks[0].dataValues));
    });
});


module.exports = router;