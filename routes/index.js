var express = require('express');
var router = express.Router();

router.get('/', function (req, res) {
  let db = req.app.get('db');
  let data = {};
  if (req.session.user != null) {
    data["auth"] = true;
    db.Institution.findAll({
      through: {
        attributes: ["Name", "URL", "EmailDomain"]
      },
      where: {
        id: req.session.user.InstitutionId
      },
      include: {
        model: db.Book,
        through: {
          attributes: ["ISBN", "Title", "Author"]
        }
      }
    }).then(InstitutionBooks => {
      data["inst"] = InstitutionBooks[0].dataValues;
      data["user"] = req.session.user;
      res.render('index', data);
    });
  } else {
    data["auth"] = false;
    res.render('index', data);
  }
});

module.exports = router;